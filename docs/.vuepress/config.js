module.exports = {
  extend: '@vuepress/theme-default',
  title: 'Jbcyrino',
  description: 'Jornalismudando',

  dest: 'public',

  themeConfig: {
    smoothScroll: true,
    nav: [
      { text: 'Currículo e premiações', link: '/cv-e-premios' },
      { text: 'Impresso', link: '/impresso/' },
      { text: 'Web', link: '/web' },
      { text: 'Rádio e TV', link: '/radio-tv' },
      { text: 'Matérias Fotográficas', link: '/materias-fotograficas' },
      { text: 'Contato', link: '/contato' }
    ]
  }
}
