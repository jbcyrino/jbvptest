---
home: true
profileImg: true
profileImgAlt: jb perfil foto
footer: O que vai no rodape?
---

Texto com descrição básico ao lado da foto. Deve ter até 300 toques e entre três a cinco linhas. O ideal é se possível sempre ser uma quebra de colunas e números ímpar.

Em dispositivos móveis o texto vai ter que ficar antes ou depois da foto. E as imagens de fundo ficam acima deles ou abaixo? Quantos caracteres tem aqui?

